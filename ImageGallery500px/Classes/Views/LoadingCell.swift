//
//  LoadingCell.swift
//  ImageGallery500px
//
//  Created by Chung on 31/05/2017.
//  Copyright © 2017 Chung. All rights reserved.
//

import UIKit

class LoadingCell: UICollectionViewCell {
    @IBOutlet weak var activity: UIActivityIndicatorView!
}
