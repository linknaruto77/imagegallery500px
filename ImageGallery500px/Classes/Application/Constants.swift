//
//  Constants.swift
//  ImageGallery500px
//
//  Created by Chung on 31/05/2017.
//  Copyright © 2017 Chung. All rights reserved.
//

import UIKit

//MARK:- Application
let application = UIApplication.shared
let appDelegate = application.delegate as! AppDelegate
let notificationCenter = NotificationCenter.default
