//
//  Configuration.swift
//  ImageGallery500px
//
//  Created by Chung on 31/05/2017.
//  Copyright © 2017 Chung. All rights reserved.
//

import UIKit

//MARK:- Localizations
func L(_ key: String?, _ args: CVarArg...) -> String {
    guard let k = key else { return "" }
    let format = NSLocalizedString(k, comment: "")
    return String(format: format, locale: Locale.current, arguments: args)
}
