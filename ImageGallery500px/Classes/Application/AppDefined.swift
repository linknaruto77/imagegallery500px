//
//  AppDefined.swift
//  ImageGallery500px
//
//  Created by Chung on 31/05/2017.
//  Copyright © 2017 Chung. All rights reserved.
//

import UIKit

private let UserDefinedKey = "User-Defined"
struct AppDefined {
    
    static func getStringValueForKey(_ key: String) -> String {
        guard let info = Bundle.main.infoDictionary else { return ""}
        return info[key] as? String ?? ""
    }
    
    static var baseApiDomain: String {
        return self.getStringValueForKey("BaseApiDomain")
    }
    
    static var consumerKey: String {
        return self.getStringValueForKey("ConsumerKey")
    }
    
}
