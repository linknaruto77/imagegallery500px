//
//  CategoryViewController.swift
//  ImageGallery500px
//
//  Created by Chung on 31/05/2017.
//  Copyright © 2017 Chung. All rights reserved.
//

import UIKit

class CategoryViewController: BaseViewController {
    @IBOutlet weak var tableView: UITableView!

    let viewModel = CategoryViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func showDetail(at indexPath: IndexPath) {
        let vc = CategoryDetailViewController.instance()
        let category = viewModel.objectAtIndexPath(indexPath)
        vc.viewModel.category = category
        navigationController?.pushViewController(vc, animated: true)
    }
    
}

//MARK:- TableView Delegate
extension CategoryViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        showDetail(at: indexPath)
    }
}

//MARK: TableView Datasource
extension CategoryViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfItemsInSection(section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell", for: indexPath) as! CategoryCell
        cell.fill(with: viewModel.cellInfoAtIndexPath(indexPath))
        return cell
    }
}

//MARK:- Storyboardable
extension CategoryViewController: Storyboardable {
    static var storyboardName: String { return "Main" }
}
