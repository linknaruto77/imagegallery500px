//
//  CategoryDetailViewController.swift
//  ImageGallery500px
//
//  Created by Chung on 31/05/2017.
//  Copyright © 2017 Chung. All rights reserved.
//

import UIKit
import Kingfisher
import DTPhotoViewerController

class CategoryDetailViewController: BaseViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    
    let viewModel = CategoryDetailViewModel()
    var refreshControl = UIRefreshControl()
    var photoViewer: PhotoViewController?
    fileprivate var selectedImageIndex: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configure()
    }
    
    private func configure() {
        self.title = viewModel.category?.name
        
        refreshControl.addTarget(self, action: #selector(refreshAction), for: .valueChanged)
        collectionView.addSubview(refreshControl)
        
        collectionView.register(UINib(nibName: "LoadingCell", bundle: nil), forCellWithReuseIdentifier: "LoadingCell")
        
        collectionView.setContentOffset(CGPoint(x: 0, y: -self.refreshControl.frame.size.height), animated: true)
        refreshControl.layoutIfNeeded()
        refreshControl.beginRefreshing()
        
        getPhotos(false)
    }
    
    func refreshAction() {
        getPhotos(false)
    }
    
    func getPhotos(_ isNext: Bool) {
        viewModel.getPhotos(isNext) { [weak self] result in
            guard let wSelf = self else { return }
            wSelf.refreshControl.endRefreshing()
            switch result {
            case .ok():
                wSelf.collectionView.reloadData()
                wSelf.photoViewer?.reloadData()
                wSelf.photoViewer?.stopLoading()
            case .failed(let error):
                UIAlertController.present(title: error?.localizedDescription, message: "", actionTitles: ["OK"])
            }
        }
    }

}

//MARK:- UICollectionView
extension CategoryDetailViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.numberOfItemsInSection(section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath == viewModel.loadingCellIndexPath {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LoadingCell", for: indexPath) as! LoadingCell
            cell.activity.startAnimating()
            return cell
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryDetailCell", for: indexPath) as! CategoryDetailCell
        cell.fill(with: viewModel.cellInfoAtIndexPath(indexPath))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if viewModel.isNext(at: indexPath) {
            getPhotos(true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath == viewModel.loadingCellIndexPath {
            return CGSize(width: collectionView.frame.size.width - 20, height: 44)
        }
        let width = (collectionView.frame.size.width / 2) - 15
        return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedImageIndex = indexPath.row
        if let cell = collectionView.cellForItem(at: indexPath) as? CategoryDetailCell {
            photoViewer = PhotoViewController(referencedView: cell.imgView, image: cell.imgView.image)
            if let photoViewer = photoViewer {
                photoViewer.dataSource = self
                photoViewer.delegate = self
                self.present(photoViewer, animated: true, completion: nil)
            }
        }
    }
    
}

//MARK:- DTPhotoViewerControllerDataSource
extension CategoryDetailViewController: DTPhotoViewerControllerDataSource {
    func photoViewerController(_ photoViewerController: DTPhotoViewerController, configureCell cell: DTPhotoCollectionViewCell, forPhotoAt index: Int) {
        if let cell = cell as? CustomPhotoCell {
            let photo = viewModel.photoList[index]
            cell.extraLabel.text = "\(photo.user?.fullname ?? "")\n\(photo.name ?? "")"
        }
    }
    
    func photoViewerController(_ photoViewerController: DTPhotoViewerController, referencedViewForPhotoAt index: Int) -> UIView? {
        let indexPath = IndexPath(item: index, section: 0)
        if let cell = collectionView?.cellForItem(at: indexPath) as? CategoryDetailCell {
            return cell.imgView
        }
        return nil
    }
    
    func numberOfItems(in photoViewerController: DTPhotoViewerController) -> Int {
        return viewModel.photoList.count
    }
    
    func photoViewerController(_ photoViewerController: DTPhotoViewerController, configurePhotoAt index: Int, withImageView imageView: UIImageView) {
        let cache = ImageCache.default
        let photo = viewModel.photoList[index]
        let largeImg = photo.images?.first?.url ?? ""
        if let image = cache.retrieveImageInMemoryCache(forKey: largeImg) {
            imageView.image = image
        } else {
            let thumb = photo.imageUrl ?? ""
            let placeholder = cache.retrieveImageInMemoryCache(forKey: thumb) ?? UIImage(named: "placeholder")
            imageView.kf.setImage(with: URL(string: largeImg),
                                  placeholder: placeholder,
                                  options: [.transition(ImageTransition.fade(1))],
                                  progressBlock: nil,
                                  completionHandler: nil)
            
        }
    }
}

//MARK:- DTPhotoViewerControllerDelegate
extension CategoryDetailViewController: DTPhotoViewerControllerDelegate {
    func photoViewerController(_ photoViewerController: DTPhotoViewerController, didScrollToPhotoAt index: Int) {
        selectedImageIndex = index
        
        // Scroll to cell position
        let indexPath = IndexPath(item: index, section: 0)
        collectionView.scrollToItem(at: indexPath, at: .bottom, animated: false)
        if (viewModel.photoList.count - 1) == index {
            photoViewer?.startLoading()
        }
    }
    
    func photoViewerControllerDidEndPresentingAnimation(_ photoViewerController: DTPhotoViewerController) {
        photoViewerController.scrollToPhoto(at: selectedImageIndex, animated: false)
        photoViewer?.showCancelButton(animated: true)
    }

    func photoViewerController(_ photoViewerController: DTPhotoViewerController, didZoomOnPhotoAtIndex: Int, atScale zoomScale: CGFloat) {
        if zoomScale == 1 {
            photoViewer?.showCancelButton(animated: true)
        } else {
            photoViewer?.hideCancelButton(animated: false)
        }
    }
    
    func photoViewerControllerDidReceiveTapGesture(_ photoViewerController: DTPhotoViewerController) {
        photoViewer?.reverseCancelButtonDisplayStatus()
    }
    
    func photoViewerControllerDidReceiveDoubleTapGesture(_ photoViewerController: DTPhotoViewerController) {
        photoViewer?.hideCancelButton(animated: false)
    }
    
    func photoViewerController(_ photoViewerController: DTPhotoViewerController, willBeginPanGestureRecognizer gestureRecognizer: UIPanGestureRecognizer) {
        photoViewer?.hideCancelButton(animated: false)
    }
}

//MARK:- Storyboardable
extension CategoryDetailViewController: Storyboardable {
    static var storyboardName: String { return "Main" }
}
