//
//  CategoryDetailViewModel.swift
//  ImageGallery500px
//
//  Created by Chung on 31/05/2017.
//  Copyright © 2017 Chung. All rights reserved.
//

import UIKit
import SwiftyJSON

class CategoryDetailViewModel: BaseViewModel {
    var category: Category?
    var photoList = [Photo]()
    var page = 1
    var haveMore = true
    var loadingCellIndexPath: IndexPath?
    
    func isNext(at indexPath: IndexPath) -> Bool {
        return (indexPath.row == photoList.count - 1) && haveMore
    }
    
    func getPhotos(_ isNext: Bool, completion:((APIResult)->())?) {
        ApiProvider.request(.photoList(page: isNext ? page : 1, catName: category?.name ?? "")) { [weak self] result in
            guard let wSelf = self else {
                completion?(.failed(error: nil))
                return
            }
            
            switch result {
            case .success(let response):
                let json = JSON(data: response.data)
                
                let photos = Photo.mapBunchToArray(json["photos"])
                if isNext {
                    wSelf.page += 1
                } else {
                    wSelf.page = 2
                    wSelf.photoList.removeAll()
                    wSelf.haveMore = true
                }
                wSelf.photoList.append(contentsOf: photos)
                
                if photos.count < 20 {
                    wSelf.haveMore = false
                }
                
                if wSelf.haveMore == true {
                    wSelf.loadingCellIndexPath = IndexPath(row: wSelf.photoList.count, section: 0)
                } else {
                    wSelf.loadingCellIndexPath = nil
                }
                
                completion?(.ok())
            case .failure(let error):
                completion?(.failed(error: error))
            }
        }
    }
}

//MARK:- UICollection Datasource
extension CategoryDetailViewModel {
    
    func numberOfItemsInSection(_ section: Int) -> Int {
        return loadingCellIndexPath == nil ? photoList.count : photoList.count + 1
    }
    
    func cellInfoAtIndexPath(_ indexPath: IndexPath) -> CategoryDetailCellInfo {
        let photo = photoList[indexPath.row]
        return CategoryDetailCellInfo(image: photo.imageUrl,
                                      author: photo.user?.fullname,
                                      title: photo.name)
    }
    
}
