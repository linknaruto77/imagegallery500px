//
//  BaseViewController.swift
//  ImageGallery500px
//
//  Created by Chung on 31/05/2017.
//  Copyright © 2017 Chung. All rights reserved.
//

import UIKit

protocol Storyboardable {
    static var storyboardName: String { get }
    static var instanceName: String { get }
}

//default name
extension Storyboardable where Self: UIViewController {
    static var storyboardName: String {
        return String(describing: Self.self)
    }
    
    static var instanceName: String {
        return String(describing: Self.self)
    }
}

extension Storyboardable where Self: UIViewController {
    
    static func instance() -> Self {
        let storyboard = UIStoryboard(name: storyboardName, bundle: Bundle.main)
        return storyboard.instantiateViewController(withIdentifier: instanceName) as! Self
    }
    
    /**
     Optional
     */
    
    static func instanceNavi() -> UINavigationController? {
        let storyboard = UIStoryboard(name: storyboardName, bundle: Bundle.main)
        return storyboard.instantiateViewController(withIdentifier: "Navi" + instanceName) as? UINavigationController
    }
    
    static func instanceSplit() -> UISplitViewController? {
        let storyboard = UIStoryboard(name: storyboardName, bundle: Bundle.main)
        return storyboard.instantiateViewController(withIdentifier: "Split" + instanceName) as? UISplitViewController
    }
}

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
}
