//
//  CategoryDetailCell.swift
//  ImageGallery500px
//
//  Created by Chung on 31/05/2017.
//  Copyright © 2017 Chung. All rights reserved.
//

import UIKit
import Kingfisher

class CategoryDetailCell: UICollectionViewCell {
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var authorLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    
    func fill(with info: CategoryDetailCellInfo) {
        if let image = info.image {
            imgView.kf.setImage(with: URL(string: image),
                              placeholder: UIImage(named: "placeholder"),
                              options: [.transition(ImageTransition.fade(1))],
                              progressBlock: nil,
                              completionHandler: nil)
        }
        
        authorLbl.text = info.author
        titleLbl.text = info.title
    }
    
}

//MARK:- binding
struct CategoryDetailCellInfo {
    let image: String?
    let author: String?
    let title: String?
}
