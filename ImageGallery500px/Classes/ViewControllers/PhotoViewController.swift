//
//  PhotoViewController.swift
//  ImageGallery500px
//
//  Created by Chung on 01/06/2017.
//  Copyright © 2017 Chung. All rights reserved.
//

import UIKit
import DTPhotoViewerController

class PhotoViewController: DTPhotoViewerController {
    var cancelButton: UIButton!
    var activityView: UIActivityIndicatorView!
    
    override init?(referencedView: UIView?, image: UIImage?) {
        super.init(referencedView: referencedView, image: image)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerClassPhotoViewer(CustomPhotoCell.self)
        
        cancelButton = UIButton(type: UIButtonType.custom)
        let image = UIImage(named: "close")
        cancelButton.setImage(image, for: UIControlState())
        cancelButton.addTarget(self, action: #selector(cancelButtonTapped(_:)), for: UIControlEvents.touchUpInside)
        cancelButton.isHidden = true
        self.view.addSubview(cancelButton)
        
        activityView = UIActivityIndicatorView(activityIndicatorStyle: .white)
        activityView.hidesWhenStopped = true
        self.view.addSubview(activityView)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        cancelButton.frame = CGRect(x: 5, y: 5, width: 40, height: 40)
        activityView.frame = CGRect(x: self.view.frame.size.width - 35, y: 15, width: 20, height: 20)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func cancelButtonTapped(_ sender: UIButton) {
        sender.isHidden = true
        self.dismiss(animated: true, completion: nil)
    }
    
    // Hide & Show info layer view
    func reverseCancelButtonDisplayStatus() {
        if self.zoomScale == 1.0 {
            if cancelButton.isHidden == true {
                showCancelButton(animated: true)
            }
            else {
                hideCancelButton(animated: true)
            }
        }
    }
    
    func hideCancelButton(animated: Bool) {
        setCancelButtonHidden(true, animated: animated)
    }
    
    func showCancelButton(animated: Bool) {
        setCancelButtonHidden(false, animated: animated)
    }
    
    fileprivate func setCancelButtonHidden(_ hidden: Bool, animated: Bool) {
        if hidden != cancelButton.isHidden {
            let duration: TimeInterval = animated ? 0.2 : 0.0
            let alpha: CGFloat = hidden ? 0.0 : 1.0
            
            // Always unhide view before animation
            cancelButton.isHidden = false
            
            UIView.animate(withDuration: duration, animations: {
                self.cancelButton.alpha = alpha
            }, completion: { (finished) in
                self.cancelButton.isHidden = hidden
            })
        }
    }
    
    func startLoading() {
        activityView.startAnimating()
    }
    
    func stopLoading() {
        activityView.stopAnimating()
    }
}

class CustomPhotoCell: DTPhotoCollectionViewCell {
    lazy var extraLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = UIColor.white
        label.font = UIFont.systemFont(ofSize: 15)
        label.numberOfLines = 0
        return label
    }()
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        addSubview(extraLabel)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let height: CGFloat = 120
        extraLabel.frame = CGRect(x: 0, y: self.bounds.size.height - height,
                                  width: self.bounds.size.width, height: height)
    }
}
