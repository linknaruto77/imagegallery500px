//
//  CategoryCell.swift
//  ImageGallery500px
//
//  Created by Chung on 31/05/2017.
//  Copyright © 2017 Chung. All rights reserved.
//

import UIKit

class CategoryCell: UITableViewCell {
    @IBOutlet weak var nameLbl: UILabel!
    
    func fill(with info: CategoryCellInfo) {
        nameLbl.text = info.name
    }
    
}

//MARK:- binding
struct CategoryCellInfo {
    let name: String?
}
