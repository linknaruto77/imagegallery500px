//
//  CategoryViewModel.swift
//  ImageGallery500px
//
//  Created by Chung on 31/05/2017.
//  Copyright © 2017 Chung. All rights reserved.
//

import UIKit

enum Category {
    case uncategorized
    case abstract
    case animals
    case blackAndWhite
    case celebrities
    case cityAndArchitechture
    case commercial
    case concert
    case family
    case fashion
    case film
    case fineArt
    case food
    case journalism
    case landscapes
    case macro
    case nature
    case nude
    case people
    case performingArts
    case sport
    case stillLife
    case street
    case transportation
    case travel
    case underwater
    case urbanExploration
    case wedding
    
    var name: String {
        switch self {
        case .uncategorized: return "Uncategorized"
        case .abstract: return "Abstract"
        case .animals: return "Animals"
        case .blackAndWhite: return "Black and White"
        case .celebrities: return "Celebrities"
        case .cityAndArchitechture: return "City and Architecture"
        case .commercial: return "Commercial"
        case .concert: return "Concert"
        case .family: return "Family"
        case .fashion: return "Fashion"
        case .film: return "Film"
        case .fineArt: return "Fine Art"
        case .food: return "Food"
        case .journalism: return "Journalism"
        case .landscapes: return "Landscapes"
        case .macro: return "Macro"
        case .nature: return "Nature"
        case .nude: return "Nude"
        case .people: return "People"
        case .performingArts: return "Performing Arts"
        case .sport: return "Sport"
        case .stillLife: return "Still Life"
        case .street: return "Street"
        case .transportation: return "Transportation"
        case .travel: return "Travel"
        case .underwater: return "Underwater"
        case .urbanExploration: return "Urban Exploration"
        case .wedding: return "Wedding"
        }
    }
}

class CategoryViewModel: BaseViewModel {
    var categoryList: [Category] = [.uncategorized, .abstract, .animals, .blackAndWhite, .celebrities, .cityAndArchitechture, .commercial, .concert, .family, .fashion, .film, .fineArt, .food, .journalism, .landscapes, .macro, .nature, .nude, .people, .performingArts, .sport, .stillLife, .street, .transportation, .travel, .underwater, .urbanExploration, .wedding]
}

//MARK:- TableView datasource
extension CategoryViewModel {
    
    func objectAtIndexPath(_ indexPath: IndexPath) -> Category {
        return categoryList[indexPath.row]
    }
    
    func cellInfoAtIndexPath(_ indexPath: IndexPath) -> CategoryCellInfo {
        return CategoryCellInfo(name: categoryList[indexPath.row].name)
    }
    
    func numberOfItemsInSection(_ section: Int) -> Int {
        return categoryList.count
    }
    
}

