//
//  Photo.swift
//  ImageGallery500px
//
//  Created by Chung on 31/05/2017.
//  Copyright © 2017 Chung. All rights reserved.
//

import UIKit
import SwiftyJSON

class Photo: BaseObject {
    
    var id: Int?
    var userId: Int?
    var name: String?
    var desc: String?
    var camera: String?
    var lens: String?
    var focalLength: String?
    var iso: String?
    var shutterSpeed: String?
    var aperture: String?
    var timesViewed: Int?
    var rating: Int?
    var status: Int?
    var createdAt: String?
    var category: Int?
    var location: String?
    var latitude: Float?
    var longitude: Float?
    var takenAt: String?
    var hiResUploaded: Int?
    var forSale: Bool?
    var width: Int?
    var height: Int?
    var votesCount: Int?
    var favoritesCount: Int?
    var commentsCoount: Int?
    var nsfw: Bool?
    var salesCount: Int?
    var forSaleDate: String?
    var highestRating: Int?
    var highestRatingDate: String?
    var licenseType: Int?
    var converted: Int?
    var collectionsCount: Int?
    var cropVersion: Int?
    var privacy: Bool?
    var profile: Bool?
    var imageUrl: String?
    var url: String?
    var positiveVotesCount: Int?
    var convertedBits: Int?
    var watermark: Bool?
    var imageFormat: String?
    var licensingRequested: Bool?
    var licensingSuggested: Bool?
    var isFreePhoto: Bool?
    var images: [Image]?
    var user: User?
    
    override func map(_ info: JSON) {
        super.map(info)
        id = info["id"].int
        userId = info["user_id"].int
        name = info["name"].string
        desc = info["description"].string
        camera = info["camera"].string
        lens = info["lens"].string
        focalLength = info["focal_length"].string
        iso = info["iso"].string
        shutterSpeed = info["shutter_speed"].string
        aperture = info["aperture"].string
        timesViewed = info["times_viewed"].int
        rating = info["rating"].int
        status = info["status"].int
        createdAt = info["created_at"].string
        category = info["category"].int
        location = info["location"].string
        latitude = info["latitude"].float
        longitude = info["longitude"].float
        takenAt = info["taken_at"].string
        hiResUploaded = info["hi_res_uploaded"].int
        forSale = info["for_sale"].bool
        width = info["width"].int
        height = info["height"].int
        votesCount = info["votes_count"].int
        favoritesCount = info["favorites_count"].int
        commentsCoount = info["comments_count"].int
        nsfw = info["nsfw"].bool
        salesCount = info["sales_count"].int
        forSaleDate = info["for_sale_date"].string
        highestRating = info["highest_rating"].int
        highestRatingDate = info["highest_rating_date"].string
        licenseType = info["license_type"].int
        converted = info["converted"].int
        collectionsCount = info["collections_count"].int
        cropVersion = info["crop_version"].int
        privacy = info["privacy"].bool
        profile = info["profile"].bool
        imageUrl = info["image_url"].string
        url = info["url"].string
        positiveVotesCount = info["positive_votes_count"].int
        converted = info["converted_bits"].int
        watermark = info["watermark"].bool
        imageFormat = info["image_format"].string
        licensingRequested = info["licensing_requested"].bool
        licensingSuggested = info["licensing_suggested"].bool
        isFreePhoto = info["is_free_photo"].bool
        images = Image.mapBunchToArray(info["images"])
        user = User.mapJsonToObject(info["user"])
    }
    
}

extension Photo {
    
    static func mapJsonToObject(_ json: JSON) -> Photo {
        let photo = Photo()
        photo.map(json)
        return photo
    }
    
    static func mapBunchToArray(_ json: JSON) -> [Photo] {
        var photos = [Photo]()
        for (_, photoJson) in json {
            let photo = Photo.mapJsonToObject(photoJson)
            photos.append(photo)
        }
        return photos
    }
    
}
