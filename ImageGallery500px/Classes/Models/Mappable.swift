//
//  Mappable.swift
//  ImageGallery500px
//
//  Created by Chung on 31/05/2017.
//  Copyright © 2017 Chung. All rights reserved.
//

import Foundation
import SwiftyJSON

//MARK:- Mappable
protocol Mappable {
    func map(_ info: JSON)
}

extension Mappable {
    func map(_ info: JSON) {
        
    }
    
    func map(_ JSONString: String?) {
        if let JSONString = JSONString {
            return map(JSONString)
        }
    }
    
    func map(_ JSONString: String) {
        do {
            guard let data = JSONString.data(using: String.Encoding.utf8) else { return }
            let dictionary = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
            map(dictionary as! JSON)
        } catch {}
    }
    
    func map(_ JSONString: NSString){
        return map(JSONString as String)
    }
    
    func map(_ JSON: AnyObject?) {
        
        if let JSON = JSON as? JSON {
            return map(JSON)
        } else if let JSON = JSON as? String {
            return map(JSON)
        }
    }
}
