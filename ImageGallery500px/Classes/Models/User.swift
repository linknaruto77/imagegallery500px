//
//  User.swift
//  ImageGallery500px
//
//  Created by Chung on 31/05/2017.
//  Copyright © 2017 Chung. All rights reserved.
//

import UIKit
import SwiftyJSON

class User: BaseObject {
    
    var id: Int?
    var username: String?
    var firstname: String?
    var lastname: String?
    var city: String?
    var country: String?
    var usertype: Int?
    var fullname: String?
    var userpicUrl: String?
    var userpicHttpsUrl: String?
    var coverUrl: String?
    var upgradeStatus: Int?
    var storeOn: Bool?
    var affection: Int?
    var avatars: Avatar?
    
    override func map(_ info: JSON) {
        super.map(info)
        id = info["id"].int
        username = info["username"].string
        firstname = info["firstname"].string
        lastname = info["lastname"].string
        city = info["city"].string
        country = info["country"].string
        usertype = info["usertype"].int
        fullname = info["fullname"].string
        userpicUrl = info["userpic_url"].string
        userpicHttpsUrl = info["userpic_https_url"].string
        coverUrl = info["cover_url"].string
        upgradeStatus = info["upgrade_status"].int
        storeOn = info["store_on"].bool
        affection = info["affection"].int
        avatars = Avatar.mapJsonToObject(info["avatars"])
    }
    
}

extension User {
    
    static func mapJsonToObject(_ json: JSON) -> User {
        let user = User()
        user.map(json)
        return user
    }
    
    static func mapBunchToArray(_ json: JSON) -> [User] {
        var users = [User]()
        for (_, userJson) in json {
            let user = User.mapJsonToObject(userJson)
            users.append(user)
        }
        return users
    }
    
}
