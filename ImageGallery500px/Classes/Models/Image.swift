//
//  Image.swift
//  ImageGallery500px
//
//  Created by Chung on 31/05/2017.
//  Copyright © 2017 Chung. All rights reserved.
//

import UIKit
import SwiftyJSON

class Image: BaseObject {
    
    var size: Int?
    var url: String?
    var httpsUrl: String?
    var format: String?
    
    override func map(_ info: JSON) {
        super.map(info)
        size = info["size"].int
        url = info["url"].string
        httpsUrl = info["https_url"].string
        format = info["format"].string
    }
    
}

extension Image {
    
    static func mapJsonToObject(_ json: JSON) -> Image {
        let image = Image()
        image.map(json)
        return image
    }
    
    static func mapBunchToArray(_ json: JSON) -> [Image] {
        var images = [Image]()
        for (_, imageJson) in json {
            let image = Image.mapJsonToObject(imageJson)
            images.append(image)
        }
        return images
    }
    
}
