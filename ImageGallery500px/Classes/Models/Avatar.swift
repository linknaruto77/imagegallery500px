//
//  Avatar.swift
//  ImageGallery500px
//
//  Created by Chung on 31/05/2017.
//  Copyright © 2017 Chung. All rights reserved.
//

import UIKit
import SwiftyJSON

class Avatar: BaseObject {
    
    var normal: String?
    var large: String?
    var small: String?
    var tiny: String?
    
    override func map(_ info: JSON) {
        super.map(info)
        normal = info["default"]["https"].string
        large = info["large"]["https"].string
        small = info["small"]["https"].string
        tiny = info["tiny"]["https"].string
    }
    
}

extension Avatar {
    
    static func mapJsonToObject(_ json: JSON) -> Avatar {
        let avatar = Avatar()
        avatar.map(json)
        return avatar
    }
    
    static func mapBunchToArray(_ json: JSON) -> [Avatar] {
        var avatars = [Avatar]()
        for (_, avatarJson) in json {
            let avatar = Avatar.mapJsonToObject(avatarJson)
            avatars.append(avatar)
        }
        return avatars
    }
    
}
