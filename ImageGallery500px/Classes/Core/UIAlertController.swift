//
//  UIAlertController.swift
//  ImageGallery500px
//
//  Created by Chung on 31/05/2017.
//  Copyright © 2017 Chung. All rights reserved.
//

import UIKit

typealias ActionHandler = (_ action: UIAlertAction) -> ()
typealias AttributedActionTitle = (title: String, style: UIAlertActionStyle)

extension UIAlertController {
    
    @discardableResult class func present(_ style: UIAlertControllerStyle = .alert,
                                          title: String?,
                                          message: String?,
                                          actionTitles: [String]?,
                                          handler: ActionHandler? = nil) -> UIAlertController? {
        
        guard let rootViewController = UIApplication.shared.delegate?.window??.rootViewController else { return nil }
        
        return self.presentFromViewController(rootViewController,
                                              style: style,
                                              title: title,
                                              message: message,
                                              actionTitles: actionTitles,
                                              handler: handler)
    }
    
    @discardableResult class func present(_ style: UIAlertControllerStyle = .alert,
                                          title: String?,
                                          message: String?,
                                          attributedActionTitles: [AttributedActionTitle]?,
                                          handler: ActionHandler? = nil) -> UIAlertController? {
        
        guard let rootViewController = UIApplication.shared.delegate?.window??.rootViewController else { return nil }
        
        return self.presentFromViewController(rootViewController,
                                              style: style,
                                              title: title,
                                              message: message,
                                              attributedActionTitles: attributedActionTitles,
                                              handler: handler)
    }
    
    @discardableResult class func presentFromViewController(_ viewController: UIViewController,
                                                            style: UIAlertControllerStyle = .alert,
                                                            title: String?,
                                                            message: String?,
                                                            actionTitles: [String]?,
                                                            handler: ActionHandler? = nil) -> UIAlertController {
        return self.presentFromViewController(viewController,
                                              style: style,
                                              title: title,
                                              message: message,
                                              attributedActionTitles: actionTitles?.map({ (title) -> AttributedActionTitle in
                                                return (title: title, style: .default)
                                              }),
                                              handler: handler)
    }
    
    @discardableResult class func presentFromViewController(_ viewController: UIViewController,
                                                            style: UIAlertControllerStyle = .alert,
                                                            title: String?,
                                                            message: String?,
                                                            attributedActionTitles: [AttributedActionTitle]?,
                                                            handler: ActionHandler? = nil) -> UIAlertController {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: style)
        
        if let _attributedActionTitles = attributedActionTitles {
            for _attributedActionTitle in _attributedActionTitles {
                let buttonAction = UIAlertAction(title: _attributedActionTitle.title, style: _attributedActionTitle.style, handler: { (action) -> Void in
                    handler?(action)
                })
                alertController.addAction(buttonAction)
            }
        }
        
        viewController.present(alertController, animated: true) {}
        return alertController
    }
}

//MARK: - Error
extension UIAlertController {
    @discardableResult class func present(_ error: Error, fromViewController: UIViewController? = nil) -> UIAlertController? {
        guard let viewController = fromViewController ?? UIApplication.shared.delegate?.window??.rootViewController
            else {
                print("CAN NOT SHOW ALERT")
                return nil
        }
        
        return self.presentFromViewController(viewController,
                                              style: .alert,
                                              title: error.localizedDescription,
                                              message: "",
                                              actionTitles: ["OK"],
                                              handler: nil)
    }
}
