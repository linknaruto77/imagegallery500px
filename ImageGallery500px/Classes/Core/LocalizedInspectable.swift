//
//  LocalizedInspectable.swift
//  ImageGallery500px
//
//  Created by Chung on 31/05/2017.
//  Copyright © 2017 Chung. All rights reserved.
//

import UIKit

@IBDesignable extension UILabel {
    @IBInspectable var localizedText: String? {
        set {
            self.text = L(newValue)
        }
        get {
            return self.text
        }
    }
}

@IBDesignable extension UIButton {
    @IBInspectable var localizedText: String? {
        set {
            self.setTitle(L(newValue), for: self.state)
        }
        get {
            return self.title(for: self.state)
        }
    }
}

@IBDesignable extension UITextField {
    @IBInspectable var localizedPlaceHolder: String? {
        set {
            self.placeholder = L(newValue)
        }
        get {
            return self.placeholder
        }
    }
}

@IBDesignable extension UITextView {
    @IBInspectable var localizedText: String? {
        set {
            self.text = L(newValue)
        }
        get {
            return self.text
        }
    }
}

@IBDesignable extension UISearchBar {
    @IBInspectable var localizedPlaceHolder: String? {
        set {
            self.placeholder = L(newValue)
        }
        get {
            return self.placeholder
        }
    }
}

@IBDesignable extension UIBarButtonItem {
    @IBInspectable var localizedText: String? {
        set {
            self.title = L(newValue)
        }
        get {
            return self.title
        }
    }
}

@IBDesignable extension UIViewController {
    @IBInspectable var localizedTitle: String? {
        set {
            self.title = L(newValue)
        }
        get {
            return self.title
        }
    }
}
