//
//  Request.swift
//  ImageGallery500px
//
//  Created by Chung on 31/05/2017.
//  Copyright © 2017 Chung. All rights reserved.
//

import Foundation
import Moya
import Result
import SwiftyJSON

enum RequestStatus {
    case ok(response: AnyObject)
    case loading
    case failed(error: NSError)
}

//MARK:- Provider

let ApiProvider = MoyaProvider<API>(endpointClosure: endpointClosure, requestClosure: endpointResolution, stubClosure: MoyaProvider.NeverStub, plugins: [RequestAlertPlugin()])

enum API {
    case photoList(page: Int, catName: String)
}

extension API: TargetType {
    var baseURL: URL {
        return URL(string: AppDefined.baseApiDomain)!
    }
    
    var path: String {
        switch self {
        case .photoList:
            return "photos"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .photoList:
            return .GET
        }
    }
    
    var parameters: [String: Any]? {
        switch self {
        case .photoList(let page, let catName):
            return ["feature" : "fresh_today", "sort" : "created_at", "image_size" : "20,5", "page" : "\(page)", "only" : catName]
//        default: return nil
        }
    }
    
    var sampleData: Data {
        var file = ""
        switch self {
        case .photoList:
            file = "mPhotoList"
        }
        
        guard let path = Bundle.main.path(forResource: file, ofType: "json"),
            let content = try? String(contentsOfFile: path),
            let data = content.data(using: String.Encoding.utf8)
            else { return Data() }
        
        return data
    }
    
    var task: Task {
        return Task.request
    }
}

extension API {
    func url() -> String {
        return baseURL.appendingPathComponent(path).absoluteString
    }
}

//MARK:- Endpoint
let endpointClosure = { (target: API) -> Endpoint<API> in
    var url = target.url()
    var params = target.parameters
    params?["consumer_key"] = AppDefined.consumerKey
    let endpoint = Endpoint<API>(URL: url, sampleResponseClosure: { o in
        print("\(o)")
        return .networkResponse(200, target.sampleData)
    }, method: target.method, parameters: params)
    
    return endpoint.endpointByAddingParameterEncoding(URLEncoding())
}

let endpointResolution: MoyaProvider<API>.RequestClosure = { endpoint, done in
    // Modify the request however you like.
    if let urlRequest = endpoint.urlRequest {
        print("\(urlRequest)")
        done(.success(urlRequest))
    } else {
//        done(.failure(Moya.Error.requestMapping(endpoint.URL)))
    }
}

//Test
let failureEndpointClosure = { (target: API) -> Endpoint<API> in
    let error = NSError(domain: "com.moya.error", code: 0, userInfo: [NSLocalizedDescriptionKey: "Houston, we have a problem"])
    var params = target.parameters
    return Endpoint<API>(URL: target.url(), sampleResponseClosure: { o in
        print(o)
        return .networkError(error)
    }, method: target.method, parameters: params)
}

let unauthorizedEndpointClosure = { (target: API) -> Endpoint<API> in
    let error = NSError(domain: "com.moya.error", code: 401, userInfo: [NSLocalizedDescriptionKey: "Houston, we are unauthorized"])
    return Endpoint<API>(URL: target.url(), sampleResponseClosure: {.networkError(error)}, method: target.method, parameters: target.parameters)
}

let successEndpointClosure = { (target: API) -> Endpoint<API> in
    return Endpoint(URL: target.url(), sampleResponseClosure: { o in
        print(o)
        return .networkResponse(200, target.sampleData)
    }, method: target.method, parameters: target.parameters)
}

final class RequestAlertPlugin: PluginType {
    
    func willSendRequest(_ request: RequestType, target: TargetType) {
    }
    
    func didReceiveResponse(_ result: Result<Moya.Response, Moya.Error>, target: TargetType) {
        switch result {
        case .success(let response):
            let json = JSON(data: response.data)
            if let status = json["status"].string, status == "denied" {
            }
            break
        case .failure(_): break
        }
    }
}
