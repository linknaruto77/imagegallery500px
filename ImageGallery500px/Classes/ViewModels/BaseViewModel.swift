//
//  BaseViewModel.swift
//  ImageGallery500px
//
//  Created by Chung on 31/05/2017.
//  Copyright © 2017 Chung. All rights reserved.
//

import UIKit

enum APIResult {
    case ok()
    case failed(error: Error?)
}

class BaseViewModel: NSObject {
}
