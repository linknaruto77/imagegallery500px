// Generated using SwiftGen, by O.Halligon — https://github.com/AliSoftware/SwiftGen

import Foundation

// swiftlint:disable file_length
// swiftlint:disable type_body_length
enum L10n {
  /// Confirm
  case confirm
  /// Cancel
  case cancel
  /// The entered email is invalid
  case emailWrongFormat
  /// Send
  case emailSendButton
  /// Log in
  case loginTitle
  /// Log in
  case loginBtn
  /// Something wrong
  case loginFailHeader
  /// For those who have Facebook accounts
  case loginFbLabel
  /// Login with a Facebook account
  case loginFbButton
  /// Please confirm
  case logoutConfirmHeader
  /// Are you sure, you want to quit?
  case logoutConfirmMessage
  /// Create order
  case menuCreateOrder
  /// Shift work
  case menuShiftWork
  /// Sales report
  case menuSalesReport
  /// Setting
  case menuSetting
  /// Create order
  case createTitle
  /// Special note
  case createSpecialNote
  /// Discount code
  case createDiscountCode
  /// Special Discount
  case createSpecialDiscount
  /// Cart
  case cartTitle
  /// Total
  case cartTotal
  /// Price
  case cartPrice
  /// VAT 7%
  case cartTax
  /// Balance to pay
  case cartBalance
  /// Shift work
  case shiftworkTitle
  /// Sales report
  case salesTitle
  /// Setting
  case settingTitle
  /// General settings
  case generalSetting
  /// Sales person
  case agentSetting
  /// Payment and Shipping
  case paymentSetting
  /// Hardware
  case hardwareSetting
  /// General settings
  case generalSettingTitle
  /// Choose operating mode
  case generalSettingChooseOperatingMode
  /// Operating mode
  case generalSettingOperatingMode
  /// Prefix
  case generalSettingPrefix
  /// Purchase order prefix
  case generalSettingPurchaseOrderPrefix
  /// Receipt prefix
  case generalSettingReceiptPrefix
  /// Receipt
  case generalSettingReceipt
  /// Print receip automatically
  case generalSettingPrintReceiptAuto
  /// Customize receipt
  case generalSettingCustomizeReceipt
  /// Sales person
  case agentSettingTitle
  /// Store administrator with primary account
  case agentSettingAccount
  /// Shopkeeper
  case agentSettingShopkeeper
  /// Sales person
  case agentSettingSalesPerson
  /// Add salesman
  case agentSettingAddSalesman
  /// Deer Bird
  case agentSettingDeerBird
  /// Payment and Shipping
  case paymentSettingTitle
  /// Hardware
  case hardwareSettingTitle
}
// swiftlint:enable type_body_length

extension L10n: CustomStringConvertible {
  var description: String { return self.string }

  var string: String {
    switch self {
      case .confirm:
        return L10n.tr(key: "confirm")
      case .cancel:
        return L10n.tr(key: "cancel")
      case .emailWrongFormat:
        return L10n.tr(key: "email_wrong_format")
      case .emailSendButton:
        return L10n.tr(key: "email_send_button")
      case .loginTitle:
        return L10n.tr(key: "login_title")
      case .loginBtn:
        return L10n.tr(key: "login_btn")
      case .loginFailHeader:
        return L10n.tr(key: "login_fail_header")
      case .loginFbLabel:
        return L10n.tr(key: "login_fb_label")
      case .loginFbButton:
        return L10n.tr(key: "login_fb_button")
      case .logoutConfirmHeader:
        return L10n.tr(key: "logout_confirm_header")
      case .logoutConfirmMessage:
        return L10n.tr(key: "logout_confirm_message")
      case .menuCreateOrder:
        return L10n.tr(key: "menu_create_order")
      case .menuShiftWork:
        return L10n.tr(key: "menu_shift_work")
      case .menuSalesReport:
        return L10n.tr(key: "menu_sales_report")
      case .menuSetting:
        return L10n.tr(key: "menu_setting")
      case .createTitle:
        return L10n.tr(key: "create_title")
      case .createSpecialNote:
        return L10n.tr(key: "create_special_note")
      case .createDiscountCode:
        return L10n.tr(key: "create_discount_code")
      case .createSpecialDiscount:
        return L10n.tr(key: "create_special_discount")
      case .cartTitle:
        return L10n.tr(key: "cart_title")
      case .cartTotal:
        return L10n.tr(key: "cart_total")
      case .cartPrice:
        return L10n.tr(key: "cart_price")
      case .cartTax:
        return L10n.tr(key: "cart_tax")
      case .cartBalance:
        return L10n.tr(key: "cart_balance")
      case .shiftworkTitle:
        return L10n.tr(key: "shiftwork_title")
      case .salesTitle:
        return L10n.tr(key: "sales_title")
      case .settingTitle:
        return L10n.tr(key: "setting_title")
      case .generalSetting:
        return L10n.tr(key: "general_setting")
      case .agentSetting:
        return L10n.tr(key: "agent_setting")
      case .paymentSetting:
        return L10n.tr(key: "payment_setting")
      case .hardwareSetting:
        return L10n.tr(key: "hardware_setting")
      case .generalSettingTitle:
        return L10n.tr(key: "general_setting_title")
      case .generalSettingChooseOperatingMode:
        return L10n.tr(key: "general_setting_choose_operating_mode")
      case .generalSettingOperatingMode:
        return L10n.tr(key: "general_setting_operating_mode")
      case .generalSettingPrefix:
        return L10n.tr(key: "general_setting_prefix")
      case .generalSettingPurchaseOrderPrefix:
        return L10n.tr(key: "general_setting_purchase_order_prefix")
      case .generalSettingReceiptPrefix:
        return L10n.tr(key: "general_setting_receipt_prefix")
      case .generalSettingReceipt:
        return L10n.tr(key: "general_setting_receipt")
      case .generalSettingPrintReceiptAuto:
        return L10n.tr(key: "general_setting_print_receipt_auto")
      case .generalSettingCustomizeReceipt:
        return L10n.tr(key: "general_setting_customize_receipt")
      case .agentSettingTitle:
        return L10n.tr(key: "agent_setting_title")
      case .agentSettingAccount:
        return L10n.tr(key: "agent_setting_account")
      case .agentSettingShopkeeper:
        return L10n.tr(key: "agent_setting_shopkeeper")
      case .agentSettingSalesPerson:
        return L10n.tr(key: "agent_setting_sales_person")
      case .agentSettingAddSalesman:
        return L10n.tr(key: "agent_setting_add_salesman")
      case .agentSettingDeerBird:
        return L10n.tr(key: "agent_setting_deer_bird")
      case .paymentSettingTitle:
        return L10n.tr(key: "payment_setting_title")
      case .hardwareSettingTitle:
        return L10n.tr(key: "hardware_setting_title")
    }
  }

  private static func tr(key: String, _ args: CVarArg...) -> String {
    let format = NSLocalizedString(key, comment: "")
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

func tr(key: L10n) -> String {
  return key.string
}
